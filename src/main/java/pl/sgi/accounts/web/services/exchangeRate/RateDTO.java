package pl.sgi.accounts.web.services.exchangeRate;

import java.math.BigDecimal;

public class RateDTO {
    private String no;
    private String effectiveDate;
    private BigDecimal mid;

    public RateDTO() {}

    public RateDTO(String no, String effectiveDate, BigDecimal mid) {
        this.no = no;
        this.effectiveDate = effectiveDate;
        this.mid = mid;
    }

    public String getNo() {
        return no;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public BigDecimal getMid() {
        return mid;
    }
}
