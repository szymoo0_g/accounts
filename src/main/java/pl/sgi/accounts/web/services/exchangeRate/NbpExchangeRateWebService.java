package pl.sgi.accounts.web.services.exchangeRate;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import pl.sgi.accounts.database.CurrencyType;

import java.math.BigDecimal;

class NbpExchangeRateWebService {

    private final RestTemplate restTemplate;
    private final String webServiceUrlPattern;

    NbpExchangeRateWebService(RestTemplate restTemplate, String webServiceUrlPattern) {
        this.restTemplate = restTemplate;
        this.webServiceUrlPattern = webServiceUrlPattern;
    }

    BigDecimal getExchangeRateInRelationToPLN(CurrencyType currency) throws HttpClientErrorException {
        if(CurrencyType.PLN.equals(currency)) {
            return BigDecimal.ONE;
        }
        ExchangeRateDTO rate = restTemplate.getForObject(
                String.format(webServiceUrlPattern, currency.toString()), ExchangeRateDTO.class);
        return rate.getRates().get(0).getMid();
    }

}
