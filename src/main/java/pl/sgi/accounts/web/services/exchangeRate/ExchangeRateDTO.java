package pl.sgi.accounts.web.services.exchangeRate;

import java.util.List;

public class ExchangeRateDTO {
    private String table;
    private String currency;
    private String code;
    private List<RateDTO> rates;

    public ExchangeRateDTO() {}

    public ExchangeRateDTO(String table, String currency, String code, List<RateDTO> rates) {
        this.table = table;
        this.currency = currency;
        this.code = code;
        this.rates = rates;
    }

    public String getTable() {
        return table;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCode() {
        return code;
    }

    public List<RateDTO> getRates() {
        return rates;
    }
}
