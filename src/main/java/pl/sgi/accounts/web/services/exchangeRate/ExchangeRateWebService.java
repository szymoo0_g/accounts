package pl.sgi.accounts.web.services.exchangeRate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import pl.sgi.accounts.database.CurrencyType;

import java.math.BigDecimal;
import java.math.MathContext;

@Service
public class ExchangeRateWebService {

    private final NbpExchangeRateWebService nbpExchangeRateWebService;
    private final MathContext financialMathContext;

    @Autowired
    public ExchangeRateWebService(
            RestTemplate restTemplate,
            @Value("${web.services.nbp.urlPattern}") String webServiceUrlPattern,
            MathContext financialMathContext) {
        this.nbpExchangeRateWebService = new NbpExchangeRateWebService(restTemplate, webServiceUrlPattern);
        this.financialMathContext = financialMathContext;
    }

    public BigDecimal getExchangeRate(CurrencyType sourceCurrency, CurrencyType destinationCurrency) throws HttpClientErrorException {
        BigDecimal sourceExchangeRateInRelationToPln = nbpExchangeRateWebService
                .getExchangeRateInRelationToPLN(sourceCurrency);
        BigDecimal destinationExchangeRateInRelationToPln = nbpExchangeRateWebService
                .getExchangeRateInRelationToPLN(destinationCurrency);
        return sourceExchangeRateInRelationToPln.divide(destinationExchangeRateInRelationToPln, financialMathContext);
    }

}
