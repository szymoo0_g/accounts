package pl.sgi.accounts.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.MathContext;
import java.math.RoundingMode;

@Configuration
class FinancialCalculationConfig {

    @Bean
    public MathContext financialMathContext() {
        return new MathContext(5, RoundingMode.HALF_DOWN);
    }

}
