package pl.sgi.accounts.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sgi.accounts.commands.create.AccountCreateDTO;
import pl.sgi.accounts.commands.create.AccountCreateException;
import pl.sgi.accounts.commands.create.AccountCreateService;
import pl.sgi.accounts.queries.details.AccountViewException;
import pl.sgi.accounts.queries.details.AccountDTO;
import pl.sgi.accounts.queries.details.AccountDetailsView;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/account")
public class AccountController {

    private final AccountDetailsView accountDetailsView;
    private final AccountCreateService accountCreateService;

    @Autowired
    AccountController(AccountDetailsView accountDetailsView, AccountCreateService accountCreateService) {
        this.accountDetailsView = accountDetailsView;
        this.accountCreateService = accountCreateService;
    }

    @GetMapping("/{accountOwnerPesel}")
    public AccountDTO getAccountDetails(
            @PathVariable(value="accountOwnerPesel", required=true) String accountOwnerPesel) throws AccountViewException {
        return accountDetailsView.getAccountDetails(accountOwnerPesel);
    }

    @PostMapping("")
    public ResponseEntity createNewAccount(
            @Valid @RequestBody AccountCreateDTO createDto) throws AccountCreateException {
        accountCreateService.createAccount(createDto);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @ExceptionHandler({ AccountViewException.class })
    public ResponseEntity handleAccountViewException(AccountViewException exception) {
        return new ResponseEntity(exception.getReason(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ AccountCreateException.class })
    public ResponseEntity handleAccountCreateException(AccountCreateException exception) {
        return new ResponseEntity(exception.getReason(), HttpStatus.BAD_REQUEST);
    }

}
