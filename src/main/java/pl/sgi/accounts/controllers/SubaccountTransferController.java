package pl.sgi.accounts.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sgi.accounts.commands.transfer.SubaccountTransferDTO;
import pl.sgi.accounts.commands.transfer.SubaccountTransferException;
import pl.sgi.accounts.commands.transfer.SubaccountTransferService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/account")
public class SubaccountTransferController {

    private final SubaccountTransferService subaccountTransferService;

    @Autowired
    public SubaccountTransferController(SubaccountTransferService subaccountTransferService) {
        this.subaccountTransferService = subaccountTransferService;
    }

    @PatchMapping("transfer")
    public void getAccountDetails(
            @Valid @RequestBody SubaccountTransferDTO transferDto) throws SubaccountTransferException {
        subaccountTransferService.transferCurrency(transferDto);
    }

    @ExceptionHandler({ SubaccountTransferException.class })
    public ResponseEntity handleSubaccountTransferException(SubaccountTransferException exception) {
        return switch (exception.getReason()) {
            case EXTERNAL_EXCHANGE_RATE_SYSTEM_ERROR -> new ResponseEntity(exception.getReason(), HttpStatus.SERVICE_UNAVAILABLE);
            case ACCOUNT_NOT_FOUND -> new ResponseEntity(exception.getReason(), HttpStatus.NOT_FOUND);
            default -> new ResponseEntity(exception.getReason(), HttpStatus.BAD_REQUEST);
        };
    }

}
