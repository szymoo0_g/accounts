package pl.sgi.accounts.database;

public enum CurrencyType {
    PLN,
    USD
}
