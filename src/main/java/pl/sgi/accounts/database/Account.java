package pl.sgi.accounts.database;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "ACCOUNT")
public class Account implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @OneToOne
    @JoinColumn(nullable=false, unique = true)
    private AppUser appUser;
    @OneToMany(mappedBy = "account", fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    private List<Subaccount> subaccounts;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public AppUser getAppUser() {
        return appUser;
    }

    public void setAppUser(AppUser appUser) {
        this.appUser = appUser;
    }

    public List<Subaccount> getSubaccounts() {
        return subaccounts;
    }

    public void setSubaccounts(List<Subaccount> subaccounts) {
        this.subaccounts = subaccounts;
    }
}
