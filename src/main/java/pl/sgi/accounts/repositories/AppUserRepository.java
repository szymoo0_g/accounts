package pl.sgi.accounts.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sgi.accounts.database.AppUser;

import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

    Optional<AppUser> getByPesel(String pesel);

}
