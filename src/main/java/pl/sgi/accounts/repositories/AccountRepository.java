package pl.sgi.accounts.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sgi.accounts.database.Account;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> getByAppUserPesel(String userPesel);

}
