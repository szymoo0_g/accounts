package pl.sgi.accounts.commands.transfer.logic;

import pl.sgi.accounts.commands.transfer.SubaccountTransferDTO;
import pl.sgi.accounts.commands.transfer.SubaccountTransferException;
import pl.sgi.accounts.commands.transfer.ports.AccountByPeselGetter;
import pl.sgi.accounts.database.Account;
import pl.sgi.accounts.database.CurrencyType;
import pl.sgi.accounts.database.Subaccount;

import java.math.BigDecimal;

public class SubaccountTransferLogic {

    private final AccountByPeselGetter accountByPeselGetter;
    private final TransferValidationLogic transferValidationLogic;

    public SubaccountTransferLogic(AccountByPeselGetter accountByPeselGetter) {
        this.accountByPeselGetter = accountByPeselGetter;
        this.transferValidationLogic = new TransferValidationLogic();
    }

    public void performTransfer(SubaccountTransferDTO transferDto, BigDecimal exchangeRate) throws SubaccountTransferException {
        transferValidationLogic.validateTransferType(transferDto);

        Account account = accountByPeselGetter.get(transferDto.getAccountOwnerPesel())
                .orElseThrow(() -> SubaccountTransferException.createAccountNotFound());
        Subaccount sourceSubaccount = getSubaccountWithCurrency(account, transferDto.getSubaccountSourceCurrency());
        Subaccount destinationSubaccount = getSubaccountWithCurrency(account, transferDto.getSubaccountDestinationCurrency());

        transferValidationLogic.validateAmountToTransfer(transferDto, sourceSubaccount);

        BigDecimal amountToTransfer = transferDto.getAmount();
        BigDecimal amountAfterMultiplyByExchangeRate = amountToTransfer.multiply(exchangeRate);
        sourceSubaccount.setValue(sourceSubaccount.getValue().subtract(amountToTransfer));
        destinationSubaccount.setValue(destinationSubaccount.getValue().add(amountAfterMultiplyByExchangeRate));
    }

    private Subaccount getSubaccountWithCurrency(Account account, CurrencyType currency) throws SubaccountTransferException {
        return account.getSubaccounts().stream()
                .filter(subaccount -> subaccount.getType().equals(currency))
                .findAny()
                .orElseThrow(() -> SubaccountTransferException.createSubaccountNotFound());
    }

}
