package pl.sgi.accounts.commands.transfer;

import pl.sgi.accounts.database.CurrencyType;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class SubaccountTransferDTO {
    @NotNull private String accountOwnerPesel;
    @NotNull @DecimalMin(value = "0.0", inclusive = false) @Digits(integer = 21, fraction = 4)
    private BigDecimal amount;
    @NotNull private CurrencyType subaccountSourceCurrency;
    @NotNull private CurrencyType subaccountDestinationCurrency;

    public SubaccountTransferDTO() {}

    public SubaccountTransferDTO(
            String accountOwnerPesel,
            BigDecimal amount,
            CurrencyType subaccountSourceCurrency,
            CurrencyType subaccountDestinationCurrency) {
        this.accountOwnerPesel = accountOwnerPesel;
        this.amount = amount;
        this.subaccountSourceCurrency = subaccountSourceCurrency;
        this.subaccountDestinationCurrency = subaccountDestinationCurrency;
    }

    public String getAccountOwnerPesel() {
        return accountOwnerPesel;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public CurrencyType getSubaccountSourceCurrency() {
        return subaccountSourceCurrency;
    }

    public CurrencyType getSubaccountDestinationCurrency() {
        return subaccountDestinationCurrency;
    }
}
