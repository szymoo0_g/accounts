package pl.sgi.accounts.commands.transfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import pl.sgi.accounts.web.services.exchangeRate.ExchangeRateWebService;

import java.math.BigDecimal;

@Service
public class SubaccountTransferService {

    private final ExchangeRateWebService exchangeRateWebService;
    private final TransferLogicService transferLogicService;

    @Autowired
    public SubaccountTransferService(
            ExchangeRateWebService exchangeRateWebService, TransferLogicService transferLogicService) {
        this.exchangeRateWebService = exchangeRateWebService;
        this.transferLogicService = transferLogicService;
    }

    @Transactional(propagation = Propagation.NEVER)
    public void transferCurrency(SubaccountTransferDTO transferDto) throws SubaccountTransferException {
        BigDecimal exchangeRate = getCurrencyExchangeRate(transferDto);
        transferLogicService.performTransfer(transferDto, exchangeRate);
    }

    private BigDecimal getCurrencyExchangeRate(SubaccountTransferDTO transferDto) throws SubaccountTransferException {
        try {
            return exchangeRateWebService.getExchangeRate(
                    transferDto.getSubaccountSourceCurrency(), transferDto.getSubaccountDestinationCurrency());
        } catch (HttpClientErrorException e) {
            throw SubaccountTransferException.createExternalExchangeRateSystemError(e);
        }
    }

}
