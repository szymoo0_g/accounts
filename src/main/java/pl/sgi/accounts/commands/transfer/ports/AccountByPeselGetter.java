package pl.sgi.accounts.commands.transfer.ports;

import pl.sgi.accounts.database.Account;

import java.util.Optional;

public interface AccountByPeselGetter {
    Optional<Account> get(String pesel);
}
