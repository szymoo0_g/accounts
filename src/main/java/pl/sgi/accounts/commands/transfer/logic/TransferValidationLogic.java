package pl.sgi.accounts.commands.transfer.logic;

import pl.sgi.accounts.commands.transfer.SubaccountTransferDTO;
import pl.sgi.accounts.commands.transfer.SubaccountTransferException;
import pl.sgi.accounts.database.Subaccount;

class TransferValidationLogic {

    void validateTransferType(SubaccountTransferDTO transferDto) throws SubaccountTransferException {
        if(transferDto.getSubaccountSourceCurrency().equals(transferDto.getSubaccountDestinationCurrency())) {
            throw SubaccountTransferException.createBadTransferType();
        }
    }

    void validateAmountToTransfer(
            SubaccountTransferDTO transferDto, Subaccount sourceSubaccount) throws SubaccountTransferException {
        if(sourceSubaccount.getValue().compareTo(transferDto.getAmount()) < 0) {
            throw SubaccountTransferException.createInsufficientFunds();
        }
    }
}
