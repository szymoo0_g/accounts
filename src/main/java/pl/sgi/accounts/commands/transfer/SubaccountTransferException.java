package pl.sgi.accounts.commands.transfer;

public class SubaccountTransferException extends Exception {

    public enum Reason {
        EXTERNAL_EXCHANGE_RATE_SYSTEM_ERROR,
        ACCOUNT_NOT_FOUND,
        SUBACCOUNT_NOT_FOUND,
        BAD_TRANSFER_TYPE,
        BAD_TRANSFER_AMOUNT,
        INSUFFICIENT_FUNDS
    }

    private final Reason reason;

    private SubaccountTransferException(Reason reason, Throwable cause) {
        super(cause);
        this.reason = reason;
    }

    public Reason getReason() {
        return reason;
    }

    public static SubaccountTransferException createExternalExchangeRateSystemError(Throwable cause) {
        return new SubaccountTransferException(Reason.EXTERNAL_EXCHANGE_RATE_SYSTEM_ERROR, cause);
    }

    public static SubaccountTransferException createAccountNotFound() {
        return new SubaccountTransferException(Reason.ACCOUNT_NOT_FOUND, null);
    }

    public static SubaccountTransferException createSubaccountNotFound() {
        return new SubaccountTransferException(Reason.SUBACCOUNT_NOT_FOUND, null);
    }

    public static SubaccountTransferException createBadTransferType() {
        return new SubaccountTransferException(Reason.BAD_TRANSFER_TYPE, null);
    }

    public static SubaccountTransferException createInsufficientFunds() {
        return new SubaccountTransferException(Reason.INSUFFICIENT_FUNDS, null);
    }

}
