package pl.sgi.accounts.commands.transfer;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import pl.sgi.accounts.commands.transfer.logic.SubaccountTransferLogic;
import pl.sgi.accounts.commands.transfer.ports.AccountByPeselGetter;

import java.math.BigDecimal;

@Service
class TransferLogicService {

    private final SubaccountTransferLogic subaccountTransferLogic;

    public TransferLogicService(AccountByPeselGetter accountByPeselGetter) {
        this.subaccountTransferLogic = new SubaccountTransferLogic(accountByPeselGetter);
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ, rollbackFor = {SubaccountTransferException.class})
    public void performTransfer(SubaccountTransferDTO transferDto, BigDecimal exchangeRate) throws SubaccountTransferException {
        subaccountTransferLogic.performTransfer(transferDto, exchangeRate);
    }

}
