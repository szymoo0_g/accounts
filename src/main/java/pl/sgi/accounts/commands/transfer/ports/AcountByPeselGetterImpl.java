package pl.sgi.accounts.commands.transfer.ports;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import pl.sgi.accounts.database.Account;
import pl.sgi.accounts.repositories.AccountRepository;

import java.util.Optional;

@Repository
class AccountByPeselGetterImpl implements AccountByPeselGetter {

    private final AccountRepository repository;

    @Autowired
    AccountByPeselGetterImpl(AccountRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<Account> get(String pesel) {
        return repository.getByAppUserPesel(pesel);
    }
}
