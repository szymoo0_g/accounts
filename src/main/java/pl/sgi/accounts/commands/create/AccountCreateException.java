package pl.sgi.accounts.commands.create;

public class AccountCreateException extends Exception {

    public enum Reason {
        FIRST_OR_LAST_NAME_BAD_LENGTH,
        BAD_PESEL_FORMAT,
        BAD_USER_AGE,
        USER_ALREADY_EXISTS
    }

    private final Reason reason;

    private AccountCreateException(Reason reason) {
        this.reason = reason;
    }

    public Reason getReason() {
        return reason;
    }

    public static AccountCreateException createFirstOrLastNameBadLength() {
        return new AccountCreateException(Reason.FIRST_OR_LAST_NAME_BAD_LENGTH);
    }

    public static AccountCreateException createBadPeselFormat() {
        return new AccountCreateException(Reason.BAD_PESEL_FORMAT);
    }

    public static AccountCreateException createBadUserAge() {
        return new AccountCreateException(Reason.BAD_USER_AGE);
    }

    public static AccountCreateException createUserAlreadyExists() {
        return new AccountCreateException(Reason.USER_ALREADY_EXISTS);
    }

}
