package pl.sgi.accounts.commands.create;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import pl.sgi.accounts.commands.create.logic.AccountCreateLogic;
import pl.sgi.accounts.commands.create.ports.UserGetter;
import pl.sgi.accounts.database.Account;
import pl.sgi.accounts.commands.create.ports.Clock;

@Service
public class AccountCreateService {

    private final AccountCreateLogic accountCreateLogic;
    private final AccountCreateRepository accountCreateRepository;

    @Autowired
    AccountCreateService(
            Clock clock,
            UserGetter userGetter,
            AccountCreateRepository accountCreateRepository) {
        this.accountCreateLogic = new AccountCreateLogic(clock, userGetter);
        this.accountCreateRepository = accountCreateRepository;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void createAccount(AccountCreateDTO createDto) throws AccountCreateException {
        Account newAccount = accountCreateLogic.createAccount(createDto);
        accountCreateRepository.persistAccount(newAccount);
    }

}
