package pl.sgi.accounts.commands.create.logic;

import pl.sgi.accounts.commands.create.AccountCreateDTO;
import pl.sgi.accounts.commands.create.AccountCreateException;
import pl.sgi.accounts.database.Account;
import pl.sgi.accounts.database.AppUser;
import pl.sgi.accounts.commands.create.ports.Clock;
import pl.sgi.accounts.commands.create.ports.UserGetter;
import pl.sgi.accounts.database.CurrencyType;

import java.math.BigDecimal;

public class AccountCreateLogic {

    private final InputDataValidationLogic inputDataValidationLogic;

    public AccountCreateLogic(Clock clock, UserGetter userGetter) {
        this.inputDataValidationLogic = new InputDataValidationLogic(clock, userGetter);
    }

    public Account createAccount(AccountCreateDTO createDto) throws AccountCreateException {
        inputDataValidationLogic.validate(createDto);
        AppUser newUser = new UserBuilder()
                .withFirstName(createDto.getFirstName())
                .withLastName(createDto.getLastName())
                .withPesel(createDto.getPesel())
                .build();
        Account newAccount = new AccountBuilder()
                .forUser(newUser)
                .addSubaccountWithTypeAndInitialValue(CurrencyType.PLN, createDto.getInitialPlnSubaccountValue())
                .addSubaccountWithTypeAndInitialValue(CurrencyType.USD, BigDecimal.ZERO)
                .build();
        return newAccount;
    }

}
