package pl.sgi.accounts.commands.create.logic;

import pl.sgi.accounts.commands.create.AccountCreateDTO;
import pl.sgi.accounts.commands.create.AccountCreateException;
import pl.sgi.accounts.commands.create.ports.Clock;
import pl.sgi.accounts.commands.create.ports.UserGetter;

import java.time.LocalDate;
import java.time.Period;

class InputDataValidationLogic {

    private final static int ADULT_AGE_IN_POLAND = 18;

    private final Clock clock;
    private final UserGetter userGetter;
    private final PeselParser peselParser;

    InputDataValidationLogic(Clock clock, UserGetter userGetter) {
        this.clock = clock;
        this.userGetter = userGetter;
        this.peselParser = new PeselParser();
    }

    void validate(AccountCreateDTO dto) throws AccountCreateException {
        validatePesel(dto);
        validateNewUserAge(dto);
        validateIfUserWithGivenPeselAlreadyDoesNotExists(dto);
    }

    private void validatePesel(AccountCreateDTO dto) throws AccountCreateException {
        String pesel = dto.getPesel();
        peselParser.validatePesel(pesel);
    }

    private void validateNewUserAge(AccountCreateDTO dto) throws AccountCreateException {
        LocalDate now = clock.getNow();
        LocalDate dateOfBirth = peselParser.getDateOfBirthFromPesel(dto.getPesel());
        Period intervalPeriod = Period.between(dateOfBirth, now);
        int yearsDifference = intervalPeriod.getYears();
        if(yearsDifference < ADULT_AGE_IN_POLAND) {
            throw AccountCreateException.createBadUserAge();
        }
    }

    private void validateIfUserWithGivenPeselAlreadyDoesNotExists(AccountCreateDTO dto) throws AccountCreateException {
        if(userGetter.getUserByPesel(dto.getPesel()).isPresent()) {
            throw AccountCreateException.createUserAlreadyExists();
        }
    }

}
