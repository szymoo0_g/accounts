package pl.sgi.accounts.commands.create.ports;

import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
class ClockImpl implements Clock {

    @Override
    public LocalDate getNow() {
        return LocalDate.now();
    }

}
