package pl.sgi.accounts.commands.create.logic;

import pl.sgi.accounts.commands.create.AccountCreateException;

import java.time.DateTimeException;
import java.time.LocalDate;

class PeselParser {

    private final static String PESEL_PATTERN = "^[0-9]{11}$";

    LocalDate getDateOfBirthFromPesel(String pesel) throws AccountCreateException {
        int peselDigits[] = new int[11];
        for (int i = 0; i < 11; i++){
            peselDigits[i] = Integer.parseInt(pesel.substring(i, i+1));
        }

        int birthYear = 10 * peselDigits[0] + peselDigits[1];
        int birthMonth = 10 * peselDigits[2] + peselDigits[3];
        int birthDay = 10 * peselDigits[4] + peselDigits[5];
        if (birthDay > 31) {
            throw AccountCreateException.createBadPeselFormat();
        }

        if (birthMonth <= 12) {
            birthYear += 1900;
        } else if (birthMonth <= 32) {
            birthYear += 2000;
            birthMonth -= 20;
        } else if (birthMonth <= 52) {
            birthYear += 2100;
            birthMonth -= 40;
        } else if (birthMonth <= 72) {
            birthYear += 2200;
            birthMonth -= 60;
        } else if (birthMonth <= 92) {
            birthYear += 1800;
            birthMonth -= 80;
        } else {
            throw AccountCreateException.createBadPeselFormat();
        }

        try {
            return LocalDate.of(birthYear, birthMonth, birthDay);
        } catch(DateTimeException e) {
            throw AccountCreateException.createBadPeselFormat();
        }
    }

    void validatePesel(String pesel) throws AccountCreateException {
        if(!pesel.matches(PESEL_PATTERN)) {
            throw AccountCreateException.createBadPeselFormat();
        }
        getDateOfBirthFromPesel(pesel);
        // TODO: perform more pesel validations if needed
    }

}
