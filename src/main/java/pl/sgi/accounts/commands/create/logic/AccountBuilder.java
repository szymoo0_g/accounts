package pl.sgi.accounts.commands.create.logic;

import pl.sgi.accounts.database.Account;
import pl.sgi.accounts.database.AppUser;
import pl.sgi.accounts.database.Subaccount;
import pl.sgi.accounts.database.CurrencyType;

import java.math.BigDecimal;
import java.util.ArrayList;

class AccountBuilder {

    private final Account account;

    AccountBuilder() {
        account = new Account();
        account.setSubaccounts(new ArrayList<>());
    }

    AccountBuilder forUser(AppUser user) {
        account.setAppUser(user);
        user.setAccount(account);
        return this;
    }

    AccountBuilder addSubaccountWithTypeAndInitialValue(CurrencyType type, BigDecimal initialValue) {
        Subaccount subaccount = new Subaccount();
        subaccount.setType(type);
        subaccount.setValue(initialValue);
        subaccount.setAccount(account);
        account.getSubaccounts().add(subaccount);
        return this;
    }

    Account build() {
        return account;
    }

}
