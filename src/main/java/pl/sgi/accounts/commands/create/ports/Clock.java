package pl.sgi.accounts.commands.create.ports;

import java.time.LocalDate;

public interface Clock {

    LocalDate getNow();

}
