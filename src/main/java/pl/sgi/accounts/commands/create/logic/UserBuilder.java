package pl.sgi.accounts.commands.create.logic;

import pl.sgi.accounts.database.AppUser;

class UserBuilder {

    private final AppUser appUser;

    UserBuilder() {
        appUser = new AppUser();
    }

    UserBuilder withFirstName(String firstName) {
        appUser.setFirstName(firstName);
        return this;
    }

    UserBuilder withLastName(String lastName) {
        appUser.setLastName(lastName);
        return this;
    }

    UserBuilder withPesel(String pesel) {
        appUser.setPesel(pesel);
        return this;
    }

    AppUser build() {
        return appUser;
    }

}
