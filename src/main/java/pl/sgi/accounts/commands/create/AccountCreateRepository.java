package pl.sgi.accounts.commands.create;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.sgi.accounts.database.Account;
import pl.sgi.accounts.repositories.AccountRepository;
import pl.sgi.accounts.repositories.AppUserRepository;

@Repository
class AccountCreateRepository {

    private final AppUserRepository appUserRepository;
    private final AccountRepository accountRepository;

    @Autowired
    AccountCreateRepository(AppUserRepository appUserRepository, AccountRepository accountRepository) {
        this.appUserRepository = appUserRepository;
        this.accountRepository = accountRepository;
    }

    void persistAccount(Account account) {
        appUserRepository.save(account.getAppUser());
        accountRepository.save(account);
    }

}
