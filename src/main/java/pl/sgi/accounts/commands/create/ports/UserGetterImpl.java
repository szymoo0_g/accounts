package pl.sgi.accounts.commands.create.ports;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.sgi.accounts.database.AppUser;
import pl.sgi.accounts.repositories.AppUserRepository;

import java.util.Optional;

@Component
class UserGetterImpl implements UserGetter {

    private final AppUserRepository appUserRepository;

    @Autowired
    UserGetterImpl(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Override
    public Optional<AppUser> getUserByPesel(String pesel) {
        return appUserRepository.getByPesel(pesel);
    }

}
