package pl.sgi.accounts.commands.create;

import javax.validation.constraints.*;
import java.math.BigDecimal;

public class AccountCreateDTO {
    @NotNull @Size(min = 2, max = 512) private String firstName;
    @NotNull @Size(min = 2, max = 512) private String lastName;
    @NotNull @Pattern(regexp = "^[0-9]{11}$") private String pesel;
    @NotNull @DecimalMin(value = "0.0", inclusive = false) @Digits(integer = 21, fraction = 4)
    private BigDecimal initialPlnSubaccountValue;

    public AccountCreateDTO() {}

    public AccountCreateDTO(String firstName, String lastName, String pesel, BigDecimal initialPlnSubaccountValue) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.pesel = pesel;
        this.initialPlnSubaccountValue = initialPlnSubaccountValue;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPesel() {
        return pesel;
    }

    public BigDecimal getInitialPlnSubaccountValue() {
        return initialPlnSubaccountValue;
    }
}
