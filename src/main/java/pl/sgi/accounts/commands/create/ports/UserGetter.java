package pl.sgi.accounts.commands.create.ports;

import pl.sgi.accounts.database.AppUser;

import java.util.Optional;

public interface UserGetter {

    Optional<AppUser> getUserByPesel(String pesel);

}
