package pl.sgi.accounts.queries.details;

import pl.sgi.accounts.database.Account;

import java.util.List;
import java.util.stream.Collectors;

public class AccountDTO {

    private final String ownerPesel;
    private final List<SubaccountDTO> subaccounts;

    public AccountDTO(Account account) {
        ownerPesel = account.getAppUser().getPesel();
        subaccounts = account.getSubaccounts().stream().map(SubaccountDTO::new).collect(Collectors.toList());
    }

    public String getOwnerPesel() {
        return ownerPesel;
    }

    public List<SubaccountDTO> getSubaccounts() {
        return subaccounts;
    }
}
