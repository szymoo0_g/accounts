package pl.sgi.accounts.queries.details;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.sgi.accounts.database.Account;
import pl.sgi.accounts.repositories.AccountRepository;

import java.util.Optional;

@Repository
public class AccountDetailsView {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountDetailsView(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public AccountDTO getAccountDetails(String accountOwnerPesel) throws AccountViewException {
        Optional<Account> byUserPesel = accountRepository.getByAppUserPesel(accountOwnerPesel);
        return byUserPesel.map(AccountDTO::new)
                .orElseThrow(() -> AccountViewException.createAccountWithGivenOwnerPeselDoNotExists());
    }

}
