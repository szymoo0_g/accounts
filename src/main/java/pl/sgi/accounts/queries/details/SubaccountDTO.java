package pl.sgi.accounts.queries.details;

import pl.sgi.accounts.database.Subaccount;
import pl.sgi.accounts.database.CurrencyType;

import java.math.BigDecimal;

public class SubaccountDTO {

    private final CurrencyType type;
    private final BigDecimal value;

    public SubaccountDTO(Subaccount subaccount) {
        type = subaccount.getType();
        value = subaccount.getValue();
    }

    public CurrencyType getType() {
        return type;
    }

    public BigDecimal getValue() {
        return value;
    }
}
