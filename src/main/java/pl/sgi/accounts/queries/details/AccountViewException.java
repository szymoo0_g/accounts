package pl.sgi.accounts.queries.details;

public class AccountViewException extends Exception {

    public enum Reason {
        ACCOUNT_WITH_GIVEN_OWNER_PESEL_DO_NOT_EXISTS
    }

    private final Reason reason;

    private AccountViewException(Reason reason) {
        this.reason = reason;
    }

    public Reason getReason() {
        return reason;
    }

    public static AccountViewException createAccountWithGivenOwnerPeselDoNotExists() {
        return new AccountViewException(Reason.ACCOUNT_WITH_GIVEN_OWNER_PESEL_DO_NOT_EXISTS);
    }

}
