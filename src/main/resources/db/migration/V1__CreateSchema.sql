-- create [dbo].[hibernate_sequence] sequence

CREATE SEQUENCE [dbo].[hibernate_sequence]
    AS [bigint]
    START WITH 1
    INCREMENT BY 1
    MINVALUE -9223372036854775808
    MAXVALUE 9223372036854775807
    CACHE
GO

-- create [dbo].[app_user] table

CREATE TABLE [dbo].[app_user]
(
    [id]         [bigint]        NOT NULL,
    [first_name] [nvarchar](512) NOT NULL,
    [last_name]  [nvarchar](512) NOT NULL,
    [pesel]      [varchar](11)   NOT NULL,
    PRIMARY KEY CLUSTERED
        (
         [id] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- create [dbo].[account] table

CREATE TABLE [dbo].[account]
(
    [id]          [bigint] NOT NULL,
    [app_user_id] [bigint] NOT NULL,
    PRIMARY KEY CLUSTERED
        (
         [id] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
    CONSTRAINT [UK_cv8fmwv3lhqj07noxdnc17p5j] UNIQUE NONCLUSTERED
        (
         [app_user_id] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[account]
    WITH CHECK ADD CONSTRAINT [FKafrffflgfjs50vsxmq6v1qdev] FOREIGN KEY ([app_user_id])
        REFERENCES [dbo].[app_user] ([id])
GO

ALTER TABLE [dbo].[account]
    CHECK CONSTRAINT [FKafrffflgfjs50vsxmq6v1qdev]
GO

-- create [dbo].[subaccount] table

CREATE TABLE [dbo].[subaccount]
(
    [id]         [bigint]         NOT NULL,
    [type]       [varchar](255)   NOT NULL,
    [value]      [numeric](21, 4) NOT NULL,
    [account_id] [bigint]         NOT NULL,
    PRIMARY KEY CLUSTERED
        (
         [id] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
    CONSTRAINT [UKo41vnmexmqugq2pd9fy4jytel] UNIQUE NONCLUSTERED
        (
         [account_id] ASC,
         [type] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[subaccount]
    WITH CHECK ADD CONSTRAINT [FKpd57kdjrk70mr9m7bagfcl1g0] FOREIGN KEY ([account_id])
        REFERENCES [dbo].[account] ([id])
GO

ALTER TABLE [dbo].[subaccount]
    CHECK CONSTRAINT [FKpd57kdjrk70mr9m7bagfcl1g0]
GO
