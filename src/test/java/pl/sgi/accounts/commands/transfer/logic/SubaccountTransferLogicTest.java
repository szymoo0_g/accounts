package pl.sgi.accounts.commands.transfer.logic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.sgi.accounts.commands.transfer.SubaccountTransferDTO;
import pl.sgi.accounts.commands.transfer.SubaccountTransferException;
import pl.sgi.accounts.commands.transfer.ports.AccountByPeselGetter;
import pl.sgi.accounts.database.Account;
import pl.sgi.accounts.database.CurrencyType;
import pl.sgi.accounts.database.Subaccount;

import java.math.BigDecimal;
import java.util.Optional;

public class SubaccountTransferLogicTest {

    private AccountByPeselGetter accountByPeselGetterMock;
    private SubaccountTransferLogic uut;

    @BeforeEach
    public void init() {
        accountByPeselGetterMock = Mockito.mock(AccountByPeselGetter.class);
        uut = new SubaccountTransferLogic(accountByPeselGetterMock);
    }

    @Test
    public void givenConvertToTheSameCurrency_thenThrowException() {
        Account testAccount = TestHelper.createAccountWithPlnAndUsdSubaccountsValues(BigDecimal.TEN, BigDecimal.ZERO);
        Mockito.doReturn(Optional.of(testAccount)).when(accountByPeselGetterMock).get("92103000000");
        SubaccountTransferDTO transferDto = new SubaccountTransferDTO(
                "92103000000", BigDecimal.ONE, CurrencyType.PLN, CurrencyType.PLN);
        BigDecimal exchangeRate = BigDecimal.TEN;

        try {
            uut.performTransfer(transferDto, exchangeRate);
            Assertions.fail("Exception not thrown");
        } catch (SubaccountTransferException e) {
            Assertions.assertEquals(SubaccountTransferException.Reason.BAD_TRANSFER_TYPE, e.getReason());
        }
    }

    @Test
    public void givenNoAccountForGivenPesel_thenThrowException() {
        Account testAccount = TestHelper.createAccountWithPlnAndUsdSubaccountsValues(BigDecimal.TEN, BigDecimal.ZERO);
        Mockito.doReturn(Optional.empty()).when(accountByPeselGetterMock).get(Mockito.any());
        SubaccountTransferDTO transferDto = new SubaccountTransferDTO(
                "92103000000", BigDecimal.ONE, CurrencyType.PLN, CurrencyType.USD);
        BigDecimal exchangeRate = BigDecimal.TEN;

        try {
            uut.performTransfer(transferDto, exchangeRate);
            Assertions.fail("Exception not thrown");
        } catch (SubaccountTransferException e) {
            Assertions.assertEquals(SubaccountTransferException.Reason.ACCOUNT_NOT_FOUND, e.getReason());
        }
    }

    @Test
    public void givenTransferMoreThanAvailableInSubaccount_thenThrowException() {
        Account testAccount = TestHelper.createAccountWithPlnAndUsdSubaccountsValues(BigDecimal.ONE, BigDecimal.ZERO);
        Mockito.doReturn(Optional.of(testAccount)).when(accountByPeselGetterMock).get("92103000000");
        SubaccountTransferDTO transferDto = new SubaccountTransferDTO(
                "92103000000", BigDecimal.TEN, CurrencyType.PLN, CurrencyType.USD);
        BigDecimal exchangeRate = BigDecimal.TEN;

        try {
            uut.performTransfer(transferDto, exchangeRate);
            Assertions.fail("Exception not thrown");
        } catch (SubaccountTransferException e) {
            Assertions.assertEquals(SubaccountTransferException.Reason.INSUFFICIENT_FUNDS, e.getReason());
        }
    }

    @Test
    public void givenAllDataValid_thenCheckResult() throws SubaccountTransferException {
        Account testAccount = TestHelper.createAccountWithPlnAndUsdSubaccountsValues(
                BigDecimal.valueOf(100), BigDecimal.valueOf(50));
        Mockito.doReturn(Optional.of(testAccount)).when(accountByPeselGetterMock).get("92103000000");
        SubaccountTransferDTO transferDto = new SubaccountTransferDTO(
                "92103000000", BigDecimal.valueOf(10), CurrencyType.PLN, CurrencyType.USD);
        BigDecimal exchangeRate = new BigDecimal("0.2");

        uut.performTransfer(transferDto, exchangeRate);

        Subaccount plnSubaccount = testAccount.getSubaccounts().stream()
                .filter(subaccount -> CurrencyType.PLN.equals(subaccount.getType()))
                .findAny()
                .get();
        Subaccount usdSubaccount = testAccount.getSubaccounts().stream()
                .filter(subaccount -> CurrencyType.USD.equals(subaccount.getType()))
                .findAny()
                .get();
        Assertions.assertTrue(plnSubaccount.getValue().compareTo(new BigDecimal("90")) == 0);
        Assertions.assertTrue(usdSubaccount.getValue().compareTo(new BigDecimal("52")) == 0);
    }

}
