package pl.sgi.accounts.commands.transfer.logic;

import pl.sgi.accounts.database.Account;
import pl.sgi.accounts.database.CurrencyType;
import pl.sgi.accounts.database.Subaccount;

import java.math.BigDecimal;
import java.util.ArrayList;

class TestHelper {

     static Account createAccountWithPlnAndUsdSubaccountsValues(BigDecimal plnValue, BigDecimal usdValue) {
         Account account = new Account();
         account.setSubaccounts(new ArrayList<>());
         addSubaccount(account, CurrencyType.PLN, plnValue);
         addSubaccount(account, CurrencyType.USD, usdValue);
         return account;
     }

    static private void addSubaccount(Account account, CurrencyType currency, BigDecimal value) {
         Subaccount subaccount = new Subaccount();
         subaccount.setType(currency);
         subaccount.setValue(value);
         subaccount.setAccount(account);
         account.getSubaccounts().add(subaccount);
     }

}
