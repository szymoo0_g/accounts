package pl.sgi.accounts.commands.create.logic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.sgi.accounts.commands.create.AccountCreateDTO;
import pl.sgi.accounts.commands.create.AccountCreateException;
import pl.sgi.accounts.commands.create.ports.Clock;
import pl.sgi.accounts.commands.create.ports.UserGetter;
import pl.sgi.accounts.database.Account;
import pl.sgi.accounts.database.AppUser;
import pl.sgi.accounts.database.CurrencyType;
import pl.sgi.accounts.database.Subaccount;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

public class AccountCreateLogicTest {

    private Clock clockMock;
    private UserGetter userGetterMock;
    private AccountCreateLogic uut;

    @BeforeEach
    public void init() {
        clockMock = Mockito.mock(Clock.class);
        userGetterMock = Mockito.mock(UserGetter.class);
        uut = new AccountCreateLogic(clockMock, userGetterMock);
    }

    @Test
    public void givenPeselTooShort_thenThrowException() {
        performPeselBadFormatTest("9210301234");
    }

    @Test
    public void givenPeselTooLong_thenThrowException() {
        performPeselBadFormatTest("921030123456");
    }

    @Test
    public void givenPeselContainsLetter_thenThrowException() {
        performPeselBadFormatTest("921030ABC4");
    }

    private void performPeselBadFormatTest(String pesel) {
        Mockito.doReturn(LocalDate.of(2020, 01, 01)).when(clockMock).getNow();
        Mockito.doReturn(Optional.empty()).when(userGetterMock).getUserByPesel(Mockito.anyString());
        AccountCreateDTO givenDto = new AccountCreateDTO(
                "TestName", "TestLastName", pesel, BigDecimal.TEN);

        try {
            uut.createAccount(givenDto);
            Assertions.fail("Exception not thrown");
        } catch (AccountCreateException e) {
            Assertions.assertEquals(AccountCreateException.Reason.BAD_PESEL_FORMAT, e.getReason());
        }
    }

    @Test
    public void givenPersonToYoung_thenThrowException() throws AccountCreateException {
        Mockito.doReturn(LocalDate.of(2000, 01, 01)).when(clockMock).getNow();
        Mockito.doReturn(Optional.empty()).when(userGetterMock).getUserByPesel(Mockito.anyString());
        AccountCreateDTO givenDto = new AccountCreateDTO(
                "TestName", "TestLastName", "82010200000", BigDecimal.TEN);

        try {
            uut.createAccount(givenDto);
            Assertions.fail("Exception not thrown");
        } catch (AccountCreateException e) {
            Assertions.assertEquals(AccountCreateException.Reason.BAD_USER_AGE, e.getReason());
        }
    }

    @Test
    public void givenPersonHasExacly18Years_thenExceptionNotThrown() throws AccountCreateException {
        Mockito.doReturn(LocalDate.of(2000, 01, 01)).when(clockMock).getNow();
        Mockito.doReturn(Optional.empty()).when(userGetterMock).getUserByPesel(Mockito.anyString());
        AccountCreateDTO givenDto = new AccountCreateDTO(
                "TestName", "TestLastName", "82010100000", BigDecimal.TEN);

        uut.createAccount(givenDto);
    }

    @Test
    public void givenAccountWithPeselAlreadyReserved_thenThrowException() throws AccountCreateException {
        Mockito.doReturn(LocalDate.of(2020, 01, 01)).when(clockMock).getNow();
        Mockito.doReturn(Optional.of(new AppUser())).when(userGetterMock).getUserByPesel("82010200000");
        AccountCreateDTO givenDto = new AccountCreateDTO(
                "TestName", "TestLastName", "82010200000", BigDecimal.TEN);

        try {
            uut.createAccount(givenDto);
            Assertions.fail("Exception not thrown");
        } catch (AccountCreateException e) {
            Assertions.assertEquals(AccountCreateException.Reason.USER_ALREADY_EXISTS, e.getReason());
        }
    }

    @Test
    public void givenAllDataValid_thenCheckResult() throws AccountCreateException {
        Mockito.doReturn(LocalDate.of(2020, 01, 01)).when(clockMock).getNow();
        Mockito.doReturn(Optional.empty()).when(userGetterMock).getUserByPesel(Mockito.any());
        AccountCreateDTO givenDto = new AccountCreateDTO(
                "TestName", "TestLastName", "82010200000", BigDecimal.TEN);

        Account account = uut.createAccount(givenDto);

        Assertions.assertEquals(account.getAppUser().getFirstName(), "TestName");
        Assertions.assertEquals(account.getAppUser().getLastName(), "TestLastName");
        Assertions.assertEquals(account.getAppUser().getPesel(), "82010200000");
        Assertions.assertEquals(account.getSubaccounts().size(), 2);
        Subaccount plnSubaccount = account.getSubaccounts().stream()
                .filter(subaccount -> CurrencyType.PLN.equals(subaccount.getType()))
                .findAny()
                .get();
        Subaccount usdSubaccount = account.getSubaccounts().stream()
                .filter(subaccount -> CurrencyType.USD.equals(subaccount.getType()))
                .findAny()
                .get();
        Assertions.assertTrue(plnSubaccount.getValue().compareTo(BigDecimal.TEN) == 0);
        Assertions.assertTrue(usdSubaccount.getValue().compareTo(BigDecimal.ZERO) == 0);
    }

}
